from sympy import Symbol, roots
from sympy.solvers.solveset import linsolve


class LinearRecurrenceSolver:

    # TODO implement the logic for solving inhomogeneous recurrences

    # you can refer to https://youtu.be/TWBB-JlmYUc

    # homogeneous linear recurrences take the form:
    # f(n) = a1 * f(n-1) + a2 * f(n-2) + ... + ad * f(n-d)
    #  => f(n) - a1 * f(n-1) + a2 * f(n-2) + ... + ad * f(n-d) = 0
    # for the a's being any constants, and d be a constant <= n
    # d is said to be the order of the recurrence.

    # Inhomogeneous linear recurrences are the same as homogeneous
    # recurrences, except that it doesn't equal 0, it equals a function, g(n).
    #  => f(n) - a1 * f(n-1) + a2 * f(n-2) + ... + ad * f(n-d) = g(n)
    # g(n) can be anything. it can be 1, n^2, 2^n, whatever.

    # the recurrence is homogeneous if g is None or evaluates to 0
    # len(boundary_conditions) should equal len(coefficients). they can
    #  have any values, the only restriction is to have them have the same length.
    def __init__(self, coefficients, boundary_conditions, g=None, n=Symbol('n')):
        self.g = g
        self.n = n
        self.root = Symbol('α')
        self.coefficients = coefficients
        self.order = self.set_order()
        self.boundary_conditions = boundary_conditions

        self.last_const_num = 1
        self.last_particular_sol_degree = 0

        self.characteristic_equation = None
        self.homogeneous_solution = None
        self.particular_solution = None
        self.general_solution = None
        self.roots = None
        self.constants_symbols = []
        self.combination = None
        self.constants = None

    def set_order(self):
        min_val = 0
        max_val = 0
        for c in self.coefficients:
            min_val = min(min_val, c[0])
            max_val = max(max_val, c[0])
        return max_val - min_val

    def set_characteristic_equation(self):
        self.characteristic_equation = self.root ** self.n
        for i in range(self.order):
            val, coefficient = self.coefficients[i]
            self.characteristic_equation -= coefficient * self.root ** (self.n + val)

    def set_roots(self):
        self.set_characteristic_equation()
        equation = self.characteristic_equation.subs(self.n, self.order)
        self.roots = roots(equation.as_poly())

    def create_next_symbol(self, append=True):
        symbol = Symbol('c' + str(self.last_const_num))
        if append:
            self.constants_symbols.append(symbol)
        self.last_const_num += 1
        return symbol

    def set_homogeneous_solution(self):
        self.set_roots()
        self.combination = 0
        for root in self.roots:
            if root == 0: continue
            repeat = self.roots[root]
            self.combination += self.create_polynomial(degree=repeat-1, append=True) * root ** self.n
        self.homogeneous_solution = self.combination
        self.general_solution = self.homogeneous_solution

    def solve_linear_system(self):
        equations = []
        for condition in self.boundary_conditions:
            equation = self.general_solution.subs(self.n, condition[0]) - condition[1]
            equations.append(equation)
        solution = linsolve(equations, self.constants_symbols)
        self.constants = solution.args[0]

    def substitute_constants(self):
        for i in range(self.order):
            self.general_solution = self.general_solution.subs(self.constants_symbols[i], self.constants[i])

    def is_homogeneous(self):
        return self.g is None or self.g == 0

    def create_polynomial(self, degree, append=False):
        result = 0
        for power in range(degree+1):
            result += self.create_next_symbol(append=append) * self.n ** power
        return result

    def mult_terms_by_polynomial(self, equation, degree):
        result = 0
        for arg in equation.args:
            result += self.create_polynomial(degree) * arg
        return result

    def set_particular_solution(self):
        pass

    def add_particular_solution(self):
        self.set_particular_solution()
        # self.general_solution += self.particular_solution

    def solve(self):
        self.set_homogeneous_solution()
        if not self.is_homogeneous():
            self.add_particular_solution()
        self.solve_linear_system()
        self.substitute_constants()
        return self.general_solution


def test():

    n = Symbol('n')

    # fibonacci
    solver = LinearRecurrenceSolver(coefficients=[(-1, 1), (-2, 1)], boundary_conditions=[(0, 0), (1, 1)])

    # towers of hanoi
    # solver = LinearRecurrenceSolver(coefficients=[(-1, 2)], boundary_conditions=[(0, 1)])

    # repeating roots
    # solver = LinearRecurrenceSolver(coefficients=[(-1, 2), (-2, -1)], boundary_conditions=[(0, 0), (1, 1)])

    # use in a probability context
    # solver = LinearRecurrenceSolver(coefficients=[(1, 1/2), (-1, 1/2)], boundary_conditions=[(0, 0), (1000, 1)])

    # g = exponential (doesn't works)
    # solver = LinearRecurrenceSolver(coefficients=[(-1, 4)], boundary_conditions=[(1, 1)], g=3**n, n=n)

    # g = polynomial (doesn't work)
    # solver = LinearRecurrenceSolver(coefficients=[(-1, 0), (-2, 1), (-3, 2)],
    #                                 boundary_conditions=[(0, 5), (1, 3), (2, 6)], g=n*n, n=n)
    # solver = LinearRecurrenceSolver(coefficients=[(-1, 1), (-2, 2)],
    #                                 boundary_conditions=[(0, 5), (1, 3)], g=2**n, n=n)

    from sympy import latex
    res = solver.solve()
    print(res)
    print(latex(res))
    print(res.subs(n, 10).evalf())


test()
