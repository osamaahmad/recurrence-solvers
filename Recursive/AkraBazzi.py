from sympy import Symbol, integrate, Rational
from sympy import lambdify
from scipy.optimize import fsolve


class AkraBazziSolver:

    # https://www.wikiwand.com/en/Akra-Bazzi_method

    def __init__(self, terms: list, g):
        self.terms = terms
        self.g = g

    def get_exponent(self):
        p = Symbol('p')
        equation = -1
        for term in self.terms:
            equation += term[0] * term[1] ** p
        func_np = lambdify(p, equation, modules=['numpy'])
        return fsolve(func_np, 0.1)[0]

    # the recurrence is Θ(result)
    def solve(self):
        p = self.get_exponent()
        u = Symbol('u')
        x = Symbol('x')
        eq = self.g / u ** (p + 1)
        return x ** p + x ** p * integrate(eq, (u, 1, x))


def test():
    g = 1
    # terms = [(3, Rational(1, 3)), (4, Rational(1, 4))]
    terms = [(2, Rational(1, 2)), (Rational(8, 9), Rational(3, 4))]
    solver = AkraBazziSolver(terms, g)
    result = solver.solve()
    print(result)
